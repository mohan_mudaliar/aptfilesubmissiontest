# APT File Submission Test Solution

## Infrastructure/Environment

* Database - A local MS SQL database has been used. The name of the DB is 'AptSubmissionTest'
* NuGet Packages used - EntityFramework, SimpleInjector
* SubmittedFiles folder - A folder called 'SubmittedFiles' has been created at the root of the API application. This houses the files that have been processed/translated.
* Front end component of solution - A 'default.html' page has been added to the root of the solution. This page contains the HTML markup and an AJAX call to the Web API controller. 

## Models and DTOs

* Model classes - Include File and Transaction. The underlying database has been created using the Entity Framework Code First workflow
* ApiResultDto - This represents the result returned by the API to the front end. The DTO comprises of the API Status code, a list of fatal errors that were encountered (that resulted in the abortion of the program), a list of transaction errors (as a result of invalid data in the .csv file) and information about the file that was processed (FileResponseDto).
* FileResponseDto - This comprises of details of the file that was processed like file name and total number of lines read. This is used in building the ApiResultDto.
* FileTranslatorResponseDto - This contains details of the transactions that were processed. It is created in the CsvFileTranslatorService.ProcessFile() and is used to build the ApiResultDto.

## Program flow 

* The user submits a file from the web interface
* The Ajax method calls the FileTranslatorsController’s action method called Submission().
* Submission method executes SubmitFile() from the CsvFileTranslatorService.
* Here the uploaded file is validated against - The file type (expected to be .csv). Needs to have all the header/column names as expected
* If the file is invalid, an exception is thrown and displayed on the web page. The process gets terminated at this point. All exceptions are reported back to the front end as a list (ApiResultDto.FatalErrors).
* If the file is valid, then then file is saved in SubmittedFiles folder.
* The file is then processed to write the file details in the database. Each line of the csv file is saved to the Transactions table.
* While saving the transactions, each transaction is validated against amount and code. Invalid transactions are not saved. Amount is mandatory and needs to be between 1 and 20000000. Code needs to be an int.
* A response is returned where the relevant HTTP status code (Ok for success and BadRequest for failure) and transactions error messages if any.
* The response is displayed on the web page.

## Services 

* FileHelperService - This service class implements the IFileHelperService interface. It contains methods to save and validate submitted files
* CsvFileTranslatorService - This service class implements the ICsvFileTranslatorService interface. It contains methods to process the submitted files. This service calls methods in the FileHelperService service to validate the file.

## Assumptions

* The user is expected to upload only a .csv file. Any other type of file will  not be processed.
* The file needs to have the header row/column names in the same order as expected.
* The file needs to be saved within the application.
* If one or more transactions (each line within the file) are invalid, they are not saved in the database. Only valid transactions are saved.
* The TotalAmount column in the Files table is populated with the summation of the amounts of the valid transactions.
* Transaction error(s) are reported back to the user.
* The 'total lines read' information that is returned back to the front end is the count of all lines read irrespective of valid or invalid transactions.

## Additional validations introduced

* The submitted file must be a .csv file
* The CSV header row of the submitted file must match the header stipulated (Code,Name,Reference,Amount). If not, the file is invalid and the operation is aborted with a FatalError.
* Transaction (each line in the submitted file) must have a code that can be converted to int 

## Note about testing

As mentioned earlier, I have not used unit testing framework as such in my previous employments. Instead of rushing and producing a sub-standard set of tests, I chose to test this solution via the manual route. The test cases are described below for your reference. Having said this, I am keen in learning unit testing using a well established framework. I am currently enrolled onto Udemy unit testing course. I am confident that in a couple of weeks’ time I will be comfortable with unit testing techniques.

## Manual test script

Test resources - A set of files with (relevant names) is added with in the 'Test resources' folder that sits at the root of the solution. 

The manual test script (Manual test script.docx) is added to the root of the source code.