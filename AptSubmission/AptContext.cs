namespace AptSubmission
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using AptSubmission.Models;

    public class AptContext : DbContext
    {
        public AptContext()
            : base("name=AptContext")
        {
        }
        
        public DbSet<File> Files { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}