﻿using System;
using System.Collections.Generic;
using IO = System.IO;
using System.Linq;
using System.Web;
using System.Configuration;
using Models = AptSubmission.Models;
using AptSubmission.Dtos;

namespace AptSubmission.Services
{
    public class CsvFileTranslatorService : IFileTranslatorService
    {
        private AptContext _context;
        private IFileHelperService _fileHelperService;

        public CsvFileTranslatorService(IFileHelperService fileHelperService)
        {
            _context = new AptContext();
            _fileHelperService = fileHelperService;
        }

        /// <summary>
        /// Main worker method in the service. 
        /// Validates the file and if valid, goes ahead to saving it and then processing it
        /// </summary>
        /// <returns></returns>
        public FileTranslatorResponseDto SubmitFile()
        {
            if (!_fileHelperService.ValidatePostedFile())
                return null;

            // Since the file has passed the validation, its now safe to save the file
            string filePath = _fileHelperService.Save();

            // Lets start the translation process
            return ProcessFile(filePath);
        }  

        /// <summary>
        /// Creates a new row in the Files table
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private Models.File CreateFileRow(string filePath)
        {
            var file = new Models.File { FileName = IO.Path.GetFileName(filePath), TotalAmount = 0.0m };            
            _context.Files.Add(file);
            return file;
        }

        /// <summary>
        /// Processes the file in the following steps 
        /// (1) Creates a row for the file in the Files table
        /// (2) Collects all the data rows (by excluding the first row which is the header) 
        /// (3) Iterates through all the data rows and checks if code and amount are valid, if valid a new Transaction row is created in the DB (referencing the file row created in step 1), if not valid, adds an error to the TransactionErrors collection of the response
        /// (4) Updates the file object with the total amount processed
        /// (5) Returns FileTranslatorResponseDto with relevant data 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private FileTranslatorResponseDto ProcessFile(string filePath)
        {
            var response = new FileTranslatorResponseDto();

            // We have gotten this far, so we can be certain that the file is valid and can be processed. So go ahead and create a row in the Files table
            var file = CreateFileRow(filePath);

            // Update the reponse with the file name
            response.FileName = file.FileName;            
            
            // Collect all the rows in the csv
            string fileData = IO.File.ReadAllText(filePath);
            var allRows = fileData.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            // Skip the first row as this is the header
            var dataRows = allRows.Skip(1).Take(allRows.Count() - 1);           

            // Add up all the amounts that have been processed
            decimal totalAmount = 0.0m;

            foreach (string row in dataRows)
            {
                if (!string.IsNullOrEmpty(row))
                {
                    var cells = row.Split(',');

                    // Check that the transaction code is a valid int AND
                    // Check that the amount specified is non-empty, is a valid decimal and is within the range (between 1 and 20000000)
                    var codeAsString = cells[0];
                    var amountAsString = cells[cells.Length - 1];

                    var transactionCodeIsValid = ValidateTransactionCode(codeAsString);
                    var transactionAmountIsValid = ValidateTransactionAmount(amountAsString, out decimal amount);

                    if (transactionCodeIsValid && transactionAmountIsValid)
                    {
                        var transaction = new Models.Transaction
                        {
                            Code = Convert.ToInt32(cells[0]),
                            Name = cells[1],
                            Reference = cells[2],
                            Amount = amount,
                            File = file
                        };
                        _context.Transactions.Add(transaction);

                        totalAmount = totalAmount + amount;
                    }
                    else
                    {
                        if (!transactionCodeIsValid)
                            response.TransactionErrors.Add($"Transaction with code '{cells[0]}' does not have a valid code and hence cannot be processed.");

                        if (!transactionAmountIsValid)
                            response.TransactionErrors.Add($"Transaction with reference '{cells[2]}' does not have a valid amount and hence cannot be processed.");                        
                    }
                    // Update the TotalLinesRead with the count of the rows of data. 
                    // It is assumed that this is the total lines of data that is 'read' and not the count of the lines of data that has been 'processed' 
                    response.TotalLinesRead++;
                }
            }
                        
            // Update the total amount in the file object 
            file.TotalAmount = totalAmount;

            _context.SaveChanges();
            return response;
        }

        /// <summary>
        /// Validates if the code is an int
        /// </summary>
        /// <param name="codeAsString"></param>
        /// <returns></returns>
        private bool ValidateTransactionCode(string codeAsString)
        {
            return int.TryParse(codeAsString, out int codeAsInt);
        }

        /// <summary>
        /// Validates if the amount is present and if it is between 1 and 20000000
        /// </summary>
        /// <param name="amountAsString"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private bool ValidateTransactionAmount(string amountAsString, out decimal amount)
        {
            return decimal.TryParse(amountAsString, out amount) &&
                amount >= 1 && amount <= 20000000;
        }
    }
};