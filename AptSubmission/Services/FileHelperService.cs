﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using IO = System.IO;

namespace AptSubmission.Services
{
    public class FileHelperService : IFileHelperService
    {
        /// <summary>
        /// Validates based on the following criteria - 
        /// (1) Is a .csv file. This service implementation works only on .csv files and hence this check.
        /// (2) Checks that the file has valid column headers ie it matches the headers as specified in "CSVColumnHeaders" app setting key. It is assumed that all .csv files will have a header.
        /// </summary>
        /// <returns></returns>
        public bool ValidatePostedFile()
        {
            // Check that the posted file is a .csv 
            var postedFile = GetPostedFile();
            string fileExtension = IO.Path.GetExtension(postedFile.FileName);
            if (!fileExtension.ToLower().Equals(".csv"))
                throw new Exception("File translator is available only for .CSV files.");
            
            // Check that the file has valid column headers ie it matches the headers as specified in "CSVColumnHeaders" app setting key
            if (!CSVFileContainsValidHeaders(postedFile))
                throw new Exception($"The posted file '{postedFile.FileName}' does not have valid header columns.");

            return true;
        }

        /// <summary>
        /// This method assumes that the CSV file will always contain column headers
        /// </summary>
        /// <returns></returns>
        private bool CSVFileContainsValidHeaders(HttpPostedFile postedFile)
        {
            var headerRow = GetCSVHeaderColumnsOfPostedFile(postedFile);
            var stipulatedHeaders = ConfigurationManager.AppSettings["CSVColumnHeaders"].ToString();

            // If the file is empty then the header columns are absent and hence return false
            if (string.IsNullOrWhiteSpace(headerRow))
                return false;

            // If the columns in the header row and the headers in the template (as specified in "CSVColumnHeaders" app setting key) match return true, else false
            return headerRow.Contains(",") &&
                headerRow.Split(',').Length == stipulatedHeaders.Split(',').Length &&
                stipulatedHeaders.ToLower().Equals(headerRow.ToLower());
        }

        /// <summary>
        /// For the specified file, get the column headers ie the first row 
        /// </summary>
        /// <returns></returns>
        private string GetCSVHeaderColumnsOfPostedFile(HttpPostedFile postedFile)
        {
            var fileData = new IO.StreamReader(postedFile.InputStream).ReadToEnd();

            var lines = fileData?.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            return lines?[0];
        }


        /// <summary>
        /// Gets the posted file from the HTTP context
        /// </summary>
        /// <returns></returns>
        private HttpPostedFile GetPostedFile()
        {
            var httpRequest = HttpContext.Current.Request;
            HttpPostedFile file = null;

            // Check if files are available
            if (httpRequest.Files.Count > 0)
            {
                // The front end HTML page is having only one file input control and hence only one file is being sent via the HttpContext.Current.Request
                // For this reason, there is no point in returning a list of paths when we know that the list will always have only one element. So, we are returning the first element of the collection
                file = httpRequest.Files[0];
            }

            return file;
        }

        /// <summary>
        /// Saves the posted file in the ~/SubmittedFiles folder
        /// </summary>
        /// <returns></returns>
        public string Save()
        {
            var postedFile = GetPostedFile();
            var filePath = HttpContext.Current.Server.MapPath($"~/SubmittedFiles/{postedFile.FileName}");
            postedFile.SaveAs(filePath);
            return filePath;
        }
    }
}