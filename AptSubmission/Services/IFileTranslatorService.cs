﻿using AptSubmission.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AptSubmission.Services
{
    public interface IFileTranslatorService
    {
        FileTranslatorResponseDto SubmitFile();
    }
}
