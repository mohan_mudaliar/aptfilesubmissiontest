﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AptSubmission.Dtos;
using AptSubmission.Services;

namespace AptSubmission.Controllers
{
    public class FileTranslatorsController : BaseApiController
    {
        private IFileTranslatorService _fileTranslatorService;

        public FileTranslatorsController(IFileTranslatorService fileTranslatorService)
        {
            _fileTranslatorService = fileTranslatorService;
        }

        /// <summary>
        /// Submits the file by invoking the SubmitFile service method
        /// If any exceptions are encountered, an HTTP Bad Request is returned with the ApiResultDto containing a collection of errors
        /// If the submission did not cause any exceptions, but there were issues with some of the transactions, then an HTTP Ok is returned with a collection of transaction errors
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Submission()
        {
            var response = new FileTranslatorResponseDto();

            try
            {
                response = _fileTranslatorService.SubmitFile();
            }
            catch (Exception exception)
            {
                return this.BuildResult(HttpStatusCode.BadRequest, new List<string> { exception.Message }, null, null);
            }

            return this.BuildResult(HttpStatusCode.OK,
                   new List<string>(),
                   response?.TransactionErrors,
                   new FileResponseDto { FileName = response.FileName, TotalLinesRead = response.TotalLinesRead });
        }
    }
}
