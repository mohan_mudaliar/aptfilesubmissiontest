﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AptSubmission.Dtos;

namespace AptSubmission.Controllers
{
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// This method is used in the child controllers to build the ApiResultDto object with the various bits it receives 
        /// </summary>
        /// <param name="httpStatusCode"></param>
        /// <param name="fatalErrors"></param>
        /// <param name="transactionErrors"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected HttpResponseMessage BuildResult(HttpStatusCode httpStatusCode, List<string> fatalErrors, List<string> transactionErrors, object data)
        {
            var result = new ApiResultDto();
            result.Data = data;            
            result.FatalErrors = fatalErrors;
            result.TransactionErrors = transactionErrors;

            // Return a 'Success' status code if there NO errors
            if (fatalErrors?.Count == 0 && transactionErrors?.Count == 0)
                result.StatusCode = Dtos.StatusCode.Success;

            // Return a 'Failure' status code if there any fatal errors (irrespective of whether there are any transaction errors)
            if (fatalErrors?.Count > 0)
                result.StatusCode = Dtos.StatusCode.Failure;
            else
            {
                // Return a 'SuccessButWithTransactionErrors' status code if there are only transaction errors 
                if (transactionErrors?.Count > 0)
                    result.StatusCode = Dtos.StatusCode.SuccessButWithTransactionErrors;
            }

            return Request.CreateResponse<ApiResultDto>(httpStatusCode, result);
        }
    }
}
