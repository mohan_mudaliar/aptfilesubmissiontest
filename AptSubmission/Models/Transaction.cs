﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AptSubmission.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public int Code { get; set; }

        public string Name { get; set; }

        public string Reference { get; set; }

        [Range(1,20000000)]
        public decimal Amount { get; set; }

        public File File { get; set; }
    }
}