﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AptSubmission.Models
{
    public class File
    {
        public int Id { get; set; }

        [Required]
        public string FileName { get; set; }

        public decimal TotalAmount { get; set; }

        public List<Transaction> Transactions { get; set; }
    }
}