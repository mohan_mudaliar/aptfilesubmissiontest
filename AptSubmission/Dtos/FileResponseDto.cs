﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AptSubmission.Dtos
{
    public class FileResponseDto
    {
        public string FileName { get; set; }
        public int TotalLinesRead { get; set; }
    }
}