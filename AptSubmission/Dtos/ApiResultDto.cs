﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AptSubmission.Dtos
{
    public enum StatusCode
    {
        Success = 1,
        Failure = 2,
        SuccessButWithTransactionErrors = 3
    }

    public class ApiResultDto
    {
        public StatusCode StatusCode { get; set; }
        public dynamic Data { get; set; }
        public List<string> FatalErrors { get; set; }
        public List<string> TransactionErrors { get; set; }

        public ApiResultDto()
        {
            this.FatalErrors = new List<string>();
            this.TransactionErrors = new List<string>();
        }
    }
}