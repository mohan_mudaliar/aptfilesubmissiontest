﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AptSubmission.Dtos
{
    public class FileTranslatorResponseDto
    {   
        public string FileName{ get; set; }
        public int TotalLinesRead { get; set; }
        public List<string> TransactionErrors { get; set; }

        public FileTranslatorResponseDto()
        {
            this.TransactionErrors = new List<string>();
        }
    }
}