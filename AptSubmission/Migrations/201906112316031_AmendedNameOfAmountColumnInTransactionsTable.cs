namespace AptSubmission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AmendedNameOfAmountColumnInTransactionsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Transactions", "Amounts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "Amounts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Transactions", "Amount");
        }
    }
}
