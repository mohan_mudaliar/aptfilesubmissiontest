// <auto-generated />
namespace AptSubmission.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AmendedNameOfAmountColumnInTransactionsTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AmendedNameOfAmountColumnInTransactionsTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201906112316031_AmendedNameOfAmountColumnInTransactionsTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
