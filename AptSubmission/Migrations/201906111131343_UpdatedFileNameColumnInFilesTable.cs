namespace AptSubmission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedFileNameColumnInFilesTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Files", "FileName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Files", "FileName", c => c.String());
        }
    }
}
